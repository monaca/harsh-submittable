import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AnimalReminderPage } from './animal-reminder';

@NgModule({
  declarations: [
    AnimalReminderPage,
  ],
  imports: [
    IonicPageModule.forChild(AnimalReminderPage),
  ],
  exports: [
    AnimalReminderPage
  ]
})
export class AnimalReminderPageModule {}

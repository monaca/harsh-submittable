import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActivityReminderPage } from './activity-reminder';

@NgModule({
  declarations: [
    ActivityReminderPage,
  ],
  imports: [
    IonicPageModule.forChild(ActivityReminderPage),
  ],
  exports: [
    ActivityReminderPage
  ]
})
export class ActivityReminderPageModule {}

import { Component } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NotifyPage } from '../notify/notify';

@IonicPage()
@Component({
  selector: 'page-crop-reminder',
  templateUrl: 'crop-reminder.html',
})
export class CropReminderPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CropReminderPage');
  }

  back(){
  	this.navCtrl.push(NotifyPage);
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CropReminderPage } from './crop-reminder';

@NgModule({
  declarations: [
    CropReminderPage,
  ],
  imports: [
    IonicPageModule.forChild(CropReminderPage),
  ],
  exports: [
    CropReminderPage
  ]
})
export class CropReminderPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, ModalController, AlertController } from 'ionic-angular';
import { AnimalReminderPage } from '../animal-reminder/animal-reminder';
import { CropReminderPage } from '../crop-reminder/crop-reminder';
import { ActivityReminderPage } from '../activity-reminder/activity-reminder';

@IonicPage()
@Component({
  selector: 'page-notify',
  templateUrl: 'notify.html',
})
export class NotifyPage {
	public pageTitle:string = "Notifications"

  constructor(
  	public navCtrl: NavController,
  	public navParams: NavParams,
  	public actionSheetCtrl:ActionSheetController,
  	public modalCtrl:ModalController,
    public alertCtrl:AlertController){}

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotifyPage');
  }


  showAdd(page){
    let modal = this.modalCtrl.create(page);
    modal.present();
  }


  addBtn(){
  	let as = this.actionSheetCtrl.create({
      title: 'What type of reminder would you like to add',
      buttons: [
        {
          text: 'Animal Reminder',
          handler: () => {
          	this.showAdd(AnimalReminderPage);
            console.log('Animal reminder clicked');
          }
        },{
          text: 'Crop Reminder',
          handler: () => {
          	this.showAdd(CropReminderPage);
            console.log('Crop clicked');
          }
        },{
          text: 'Activity',
          handler: () => {
          	this.showAdd(ActivityReminderPage);
            console.log('Activity clicked');
          }
        }
      ]
    });
    as.present();
  }

  doMute(){
    let alert = this.alertCtrl.create({
      title: 'Hang on!',
      subTitle: 'Are you sure you want to mute all notifications from {{farmName}}?',
      buttons: ['CANCEL','OK']
    });
    alert.present();
  }

}
